//
// Created by Jonathan Valle on 2019-03-10.
//

#ifndef PROJETCOMPLEXITE_LISTECHAINEE_H
#define PROJETCOMPLEXITE_LISTECHAINEE_H

typedef struct Element{
    int nombre;
    struct Element *suivant;
}element;

typedef struct Liste{
    element *tete;
    int taille;
}Liste;

typedef struct ElementD{
    int nombre;
    struct ElementD *suivant;
    struct ElementD *precedent;
}elementD;

typedef struct ListeD{
    elementD *tete;
    elementD *queue;
    int taille;
}ListeD;

void Init(Liste *l);
void ajout_tete(Liste *l, int nb);
void affichage(Liste *l);

void InitD(ListeD *l);
void ajout_teteD(ListeD *l, int nb);
void ajout_queueD(ListeD *l, int nb);
void affichageDtete(ListeD *l);
void affichageDqueue(ListeD *l);

#endif //PROJETCOMPLEXITE_LISTECHAINEE_H
