//
// Created by Jonathan Valle on 2019-03-10.
//

#ifndef PROJETCOMPLEXITE_TRIBULLE_H
#define PROJETCOMPLEXITE_TRIBULLE_H

#include "ListeChainee.h"

void *triBulleIteratif(Liste *l);
void *triBulleRecursif(Liste *l, int taille);

#endif //PROJETCOMPLEXITE_TRIBULLE_H
