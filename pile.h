//
// Created by Jonathan Valle on 2019-03-10.
//

#ifndef PROJETCOMPLEXITE_PILE_H
#define PROJETCOMPLEXITE_PILE_H

#include "ListeChainee.h"

typedef struct Stack{
    element *premier;
}stack;

void push(stack *st, int nb);
int pop(stack *st);
void affichage_pile(stack *st);
int taille_pile(stack *p1);

#endif //PROJETCOMPLEXITE_PILE_H
