//
// Created by Jonathan Valle on 2019-03-10.
//
#include "pile.h"
#include <stdlib.h>

int tete_pile(stack *st){
    element *depile = st->premier;
    return depile==NULL ? -1 : depile->nombre;
}

stack *plus_petit_disque(stack *p1,stack *p2,stack *p3){
    element *elt1 = p1->premier;
    element *elt2 = p2->premier;
    element *elt3 = p3->premier;

    int tete1=-1;
    int tete2=-1;
    int tete3=-1;

    if(elt1!=NULL){
        tete1=tete_pile(p1);
    }
    if(elt2!=NULL){
        tete2=tete_pile(p2);
    }
    if(elt3!=NULL){
        tete3=tete_pile(p3);
    }
    if(tete1>=0){
        if(tete1<tete2 || tete2==-1){
            if(tete1<tete3 || tete3==-1){
                return p1;
            }
        }
    }

    if(tete2>=0){
        if(tete2<tete1 || tete1==-1){
            if(tete2<tete3 || tete3==-1){
                return p2;
            }
        }
    }

    if(tete3>=0){
        if(tete3<tete2 || tete2==-1){
            if(tete3<tete1 || tete1==-1){
                return p3;
            }
        }
    }
    return NULL;
}

void hanoi_recursif(int n, stack *d, stack *a, stack *m, stack *pile, stack *pile2, stack *pile3){

    int nb;

    if(n==1){
        //affichage_tour(pile,pile2,pile3);
        // deplacer le disque de la tour de départ à la tour d'arrivée
        nb = pop(d);
        push(a,nb);
        return ;
    }

    hanoi_recursif(n-1,d,m,a,pile,pile2,pile3);

    //affichage_tour(pile,pile2,pile3);
    // deplacer le disque de la tour de départ à la tour d'arrivée
    nb = pop(d);
    push(a,nb);

    hanoi_recursif(n-1,m,a,d,pile,pile2,pile3);

}

void hanoi_iteratif(stack *d, stack *m, stack *a){

    while (taille_pile(d)!=0 || (taille_pile(m)!=0 && taille_pile(a)!=0)){
        stack *tmp=malloc(sizeof(*tmp));
        tmp->premier=NULL;
        tmp=plus_petit_disque(d,m,a);
        if(tmp==d){
            int nb = pop(d);
            push(m,nb);
            //affichage_tour(d,m,a);
            if(tete_pile(a)==-1){
                if(tete_pile(d)!=-1){
                    nb=pop(d);
                    push(a,nb);
                }
            }else if(tete_pile(d)==-1){
                if(tete_pile(a)!=-1){
                    nb=pop(a);
                    push(d,nb);
                }
            }
            else if(tete_pile(a)<tete_pile(d)){
                if(tete_pile(a)!=-1){
                    nb=pop(a);
                    push(d,nb);
                }
            } else{
                if(tete_pile(d)!=-1){
                    nb=pop(d);
                    push(a,nb);
                }
            }
        }
        if(tmp==m){
            int nb = pop(m);
            push(a,nb);
            //affichage_tour(d,m,a);
            if(taille_pile(d)==-1){
                if(tete_pile(m)!=-1){
                    nb=pop(m);
                    push(d,nb);
                }
            }else if(tete_pile(m)==-1){
                if(tete_pile(d)!=-1){
                    nb=pop(d);
                    push(m,nb);
                }
            }else if(tete_pile(d)<tete_pile(m)){
                if(tete_pile(d)!=-1){
                    nb=pop(d);
                    push(m,nb);
                }
            } else{
                if(tete_pile(m)!=-1){
                    nb=pop(m);
                    push(d,nb);
                }
            }
        }
        if(tmp==a){
            int nb = pop(a);
            push(d,nb);
            //affichage_tour(d,m,a);
            if(taille_pile(m)==-1){
                if(tete_pile(a)!=-1){
                    nb=pop(a);
                    push(m,nb);
                }
            }else if(tete_pile(a)==-1){
                if(tete_pile(m)!=-1){
                    nb=pop(m);
                    push(a,nb);
                }
            }else if(tete_pile(m)<tete_pile(a)){
                if(tete_pile(m)!=-1){
                    nb=pop(m);
                    push(a,nb);
                }
            } else{
                if(tete_pile(a)!=-1){
                    nb=pop(a);
                    push(m,nb);
                }
            }
        }
        //affichage_tour(d,m,a);
    }
}

