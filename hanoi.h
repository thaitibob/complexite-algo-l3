//
// Created by Jonathan Valle on 2019-03-10.
//

#ifndef PROJETCOMPLEXITE_HANOI_H
#define PROJETCOMPLEXITE_HANOI_H

#include "pile.h"

int tete_pile(stack *st);
stack *plus_petit_disque(stack *p1,stack *p2,stack *p3);
void hanoi_recursif(int n, stack *d, stack *a, stack *m, stack *pile, stack *pile2, stack *pile3);
void hanoi_iteratif(stack *d, stack *m, stack *a);

#endif //PROJETCOMPLEXITE_HANOI_H
