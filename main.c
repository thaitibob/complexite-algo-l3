#include <stdio.h>
#include <stdlib.h>

#include "pile.h"
#include "ListeChainee.h"

#include "triBulle.h"
#include "hanoi.h"

void test_hanoi(){

    /*
    for(int z=0 ; z<3 ; z++){
        for(int j=1; j<=30 ; j++){
            int n;

            double temps;
            clock_t start;

            stack *pile=malloc(sizeof(*pile));
            pile->premier=NULL;

            stack *pile2=malloc(sizeof(*pile2));
            pile2->premier=NULL;

            stack *pile3=malloc(sizeof(*pile3));
            pile3->premier=NULL;

            //printf("nombre de tours : \n");
            //scanf("%d",&n);

            for(int i=j ; i>0 ; i--){
                push(pile,i);
            }

            //affichage_tour(pile,pile2,pile3);
            start = clock();
            hanoi_iteratif(pile,pile2,pile3);
            temps = (double)(clock()-start)/(double)CLOCKS_PER_SEC;
            printf("execution pour %d tours",j);
            printf("\nRecherche terminée en %.5f seconde(s)!\n", temps);
        }
    }
     */
}

void test_tri(){
    /*

    for(int i=100 ; i<10000000000000 ; i+=3000){

        Liste *l,*l1;
        l=Init(l);
        l1=Init(l1);

        double temps;
        clock_t start;

        int nb=0;

        for(int j=0 ; j<i ; j++){
            nb=rand();
            ajout_tete(l,nb);
            ajout_tete(l1,nb);
        }

        start = clock();
        triSelectionRecursif(l);
        temps = (double)(clock()-start)/(double)CLOCKS_PER_SEC;
        printf("execution pour %d donnees recursif",i);
        printf("\ntri terminé en %.5f seconde(s)!\n", temps);

        double temps2;
        clock_t start2;
        start2 = clock();
        triSelectionIteratif(l1);
        temps2 = (double)(clock()-start2)/(double)CLOCKS_PER_SEC;
        printf("execution pour %d donnees iteratif",i);
        printf("\ntri terminé en %.5f seconde(s)!\n", temps2);


    }*/
}

int main() {
    //test_tri();
    //test_hanoi();

    ListeD *l;
    InitD(l);
    ajout_teteD(l,20);
    ajout_teteD(l,2);
    ajout_teteD(l,8);
    ajout_teteD(l,7);
    ajout_teteD(l,11);
    ajout_teteD(l,15);
    ajout_teteD(l,17);
    ajout_teteD(l,18);

    affichageDtete(l);
    affichageDqueue(l);


    return 0;
}