//
// Created by Jonathan Valle on 2019-03-10.
//
#include "stdio.h"
#include "stdlib.h"
#include "ListeChainee.h"

void triSelectionRecursif(Liste *l){
    element *courant=l->tete;
    if(courant->suivant==NULL){
        return ;
    }
    element *min=courant;
    element *pp = min;

    while(min->suivant!=NULL){
        if(min->suivant->nombre<pp->nombre){
            pp=min->suivant;
        }
        min=min->suivant;
    }
    int tmp=pp->nombre;
    pp->nombre = courant->nombre;
    courant->nombre = tmp;

    Liste *l1 = malloc(sizeof(*l1));
    l1->tete=courant->suivant;
    triSelectionRecursif(l1);
}

void triSelectionIteratif(Liste *l){
    element *courant=l->tete;

    while(courant->suivant!=NULL){
        element *min=courant;
        element *pp = min;

        while (min->suivant!=NULL){
            if(min->suivant->nombre<pp->nombre){
                pp=min->suivant;
            }
            min=min->suivant;
        }

        int tmp=pp->nombre;
        pp->nombre = courant->nombre;
        courant->nombre = tmp;

        courant=courant->suivant;
    }
}

