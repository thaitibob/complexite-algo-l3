//
// Created by Jonathan Valle on 2019-03-10.
//

#ifndef PROJETCOMPLEXITE_TRISELECTION_H
#define PROJETCOMPLEXITE_TRISELECTION_H

#include "ListeChainee.h"

void triSelectionRecursif(Liste *l);
void triSelectionIteratif(Liste *l);

#endif //PROJETCOMPLEXITE_TRISELECTION_H
