//
// Created by Jonathan Valle on 2019-03-10.
//

#include <stdlib.h>
#include <stdio.h>
#include "ListeChainee.h"

void Init(Liste *l){
    l=malloc(sizeof(*l));
    l->tete=NULL;
    l->taille=0;
}

void ajout_tete(Liste *l, int nb){
    element *nouveau = malloc(sizeof(*nouveau));
    nouveau->nombre = nb;
    nouveau->suivant=l->tete;
    l->tete=nouveau;
    l->taille+=1;
}

void affichage(Liste *l){
    element *courant = l->tete;
    while (courant!=NULL){
        printf("element : %d \n",courant->nombre);
        courant=courant->suivant;
    }
    printf("\n");
}

void InitD(ListeD *l){
    l=malloc(sizeof(*l));
    l->tete=NULL;
    l->queue=NULL;
    l->taille=0;
}

void ajout_teteD(ListeD *l, int nb){
    elementD *nouveau = malloc(sizeof(*nouveau));
    nouveau->precedent=NULL;
    nouveau->nombre=nb;
    if (l->tete == NULL)
    {
        nouveau->suivant = NULL;
        l->tete = nouveau;
        l->queue = nouveau;
    }
    else
    {
        l->tete->precedent = nouveau;
        nouveau->suivant = l->tete;
        l->tete = nouveau;
    }
    l->taille++;
}

void ajout_queueD(ListeD *l, int nb){
    elementD *nouveau = malloc(sizeof(*nouveau));
    nouveau->suivant=NULL;
    nouveau->nombre=nb;
    if (l->queue == NULL)
    {
        nouveau->precedent = NULL;
        l->tete = nouveau;
        l->queue = nouveau;
    }
    else
    {
        l->queue->suivant = nouveau;
        nouveau->precedent = l->queue;
        l->queue = nouveau;
    }
    l->taille++;
}

void affichageDtete(ListeD *l){
    elementD *courant = l->tete;
    while (courant!=NULL){
        printf("element : %d \n",courant->nombre);
        courant=courant->suivant;
    }
    printf("\n");
}

void affichageDqueue(ListeD *l){
    elementD *courant = l->queue;
    while (courant!=NULL){
        printf("element : %d \n",courant->nombre);
        courant=courant->precedent;
    }
    printf("\n");
}

