//
// Created by Jonathan Valle on 2019-03-10.
//
#include <stdio.h>
#include "ListeChainee.h"

void *triBulleIteratif(Liste *l){
    int d=l->taille;
    for(int i=d-1 ; i>0 ; i--){
        element *courant=NULL;
        courant=l->tete;
        for(int j=0 ; j<i ; j++){
            if((courant->suivant)->nombre<courant->nombre){
                int tmp= courant->nombre;
                courant->nombre=courant->suivant->nombre;
                courant->suivant->nombre=tmp;
            }
            courant=courant->suivant;
        }
    }
    return l;
}

void *triBulleRecursif(Liste *l, int taille){
    if(taille==1){
        return l;
    }
    element *courant=NULL;
    courant=l->tete;
    for(int i=0 ; i<taille-1 ; i++){
        if(courant->nombre>courant->suivant->nombre){
            int tmp=courant->nombre;
            courant->nombre=courant->suivant->nombre;
            courant->suivant->nombre=tmp;
        }
        courant=courant->suivant;
    }
    l=triBulleRecursif(l,taille-1);
    return l;
}

