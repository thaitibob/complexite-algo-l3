//
// Created by Jonathan Valle on 2019-03-10.
//
#include <stdio.h>
#include <stdlib.h>
#include "pile.h"

void push(stack *st, int nb){
    element *nouveau = malloc(sizeof(*nouveau));
    nouveau->nombre=nb;
    nouveau->suivant=st->premier;
    st->premier=nouveau;
}

int pop(stack *st){
    int nb;
    element *depile = st->premier;
    nb=depile->nombre;
    st->premier = depile->suivant;
    free(depile);
    return nb;
}

void affichage_pile(stack *st){
    element *elt = st->premier;
    while (elt!=NULL){
        printf("%d\n",elt->nombre);
        elt=elt->suivant;
    }
    printf("\n");
}

int taille_pile(stack *p1){
    element *elt = p1->premier;
    int n=0;
    while (elt!=NULL){
        n++;
        elt=elt->suivant;
    }
    return n;
}

